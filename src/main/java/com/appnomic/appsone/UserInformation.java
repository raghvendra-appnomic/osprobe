package com.appnomic.appsone;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
public class UserInformation {

    private static final Logger log = LoggerFactory.getLogger(UserInformation.class);

    private String userName;
    private String userCountry;
    private String userLanguage;
    private String userHomeDirectory;
    private String userWorkingDirectory;
    private String userTimezone;

    public UserInformation() {
        this.userName = System.getProperty("user.name");
        log.info("userName: {} ", userName);
        this.userCountry = System.getProperty("user.country");
        log.info("userCountry: {} ", userCountry);
        this.userLanguage = System.getProperty("user.language");
        log.info("userLanguage: {} ", userLanguage);
        this.userHomeDirectory = System.getProperty("user.home");
        log.info("userHomeDirectory: {} ", userHomeDirectory);
        this.userWorkingDirectory = System.getProperty("user.dir");
        log.info("userWorkingDirectory: {} ", userWorkingDirectory);
        this.userTimezone = System.getProperty("user.timezone");
        log.info("userTimezone: {} ", userTimezone);
    }
}
