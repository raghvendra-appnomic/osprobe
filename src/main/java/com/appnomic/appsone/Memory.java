package com.appnomic.appsone;

import lombok.Data;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
public class Memory {

    private static final Logger log = LoggerFactory.getLogger(Memory.class);

    private long totalRAM;
    private long totalRAMUsed;
    private long totalRAMFree;

    private long totalSwapSpace;
    private long totalSwapSpaceUsed;
    private long totalSwapSpaceFree;

    public Memory(Sigar sigar) {
        try {
            totalRAM = sigar.getMem().getTotal();
            log.info("Total RAM allocated: {} ", totalRAM);
            totalRAMUsed = sigar.getMem().getUsed();
            log.info("Total RAM Used: {} ", totalRAMUsed);
            totalRAMFree = sigar.getMem().getFree();
            log.info("Total RAM Free: {} ", totalRAMFree);

            totalSwapSpace = sigar.getSwap().getTotal();
            log.info("Total SWAP space allocated: {} ", totalSwapSpace);
            totalSwapSpaceUsed = sigar.getSwap().getUsed();
            log.info("Total SWAP space used: {} ", totalSwapSpaceUsed);
            totalSwapSpaceFree = sigar.getSwap().getFree();
            log.info("Total SWAP space free: {} ", totalSwapSpaceFree);

        } catch (SigarException e) {
            log.error("Failed to retrieve memory information: {} ", e.getMessage());
        }
    }
}