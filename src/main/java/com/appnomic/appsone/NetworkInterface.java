package com.appnomic.appsone;

import lombok.Data;

@Data
public class NetworkInterface {
    private String name;
    private String type;
    private String address;

    public String toString() {
        return "[ " + name + "," + type + "," + address + " ]";
    }
}
