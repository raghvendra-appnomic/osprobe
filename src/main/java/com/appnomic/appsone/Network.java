package com.appnomic.appsone;

import org.hyperic.sigar.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Network {

    private static final Logger log = LoggerFactory.getLogger(Network.class);

    private NetConnection[] tcpConnections;
    private NetConnection[] udpConnections;

    private String fQDN;
    private String defaultGateway;
    private String domainName;

    private int totalInboundConnections;
    private int totalOutboundConnections;
    private int totalInboundTCPConnections;
    private int totalOutboundTCPConnections;
    private int totalEstablishedTCPConnections;
    private int totalClosedTCPConnections;

    private NetInfo netInfo;
    private NetStat netstat;

    public Network(Sigar sigar) {
        try {
            fQDN = sigar.getFQDN();
            log.info("fQDN: {} ", fQDN);
        } catch (SigarException e) {
            log.error("Failed to retrieve fQDN: {} ", e.getMessage());
        }
        try {
            netInfo = sigar.getNetInfo();
        } catch (SigarException e) {
            log.error("Failed to retrieve NetInfo: {} ", e.getMessage());
        }
        try {
            netstat = sigar.getNetStat();
        } catch (SigarException e) {
            log.error("Failed to retrieve NetStat: {} ", e.getMessage());
        }

        defaultGateway = netInfo.getDefaultGateway();
        log.info("defaultGateway: {} ", defaultGateway);

        domainName = netInfo.getDomainName();
        log.info("domainName: {} ", domainName);

        totalInboundConnections = netstat.getAllInboundTotal();
        log.info("NetStat: Total inbound connections: {} ", totalInboundConnections);
        totalOutboundConnections = netstat.getAllOutboundTotal();
        log.info("NetStat: Total outbound connections: {} ", totalOutboundConnections);
        totalInboundTCPConnections = netstat.getTcpInboundTotal();
        log.info("NetStat: Total TCP inbound connections: ", totalInboundTCPConnections);
        totalOutboundTCPConnections = netstat.getTcpOutboundTotal();
        log.info("NetStat: Total TCP outbound connections: ", totalOutboundTCPConnections);
        totalEstablishedTCPConnections = netstat.getTcpEstablished();
        log.info("NetStat: Total TCP established connections: " + totalEstablishedTCPConnections);
        totalClosedTCPConnections = netstat.getTcpClose();
        log.info("NetStat: Total TCP closed connections: " + totalClosedTCPConnections);

        tcpConnections = getTcpConnections(sigar);
        udpConnections = getUdpConnections(sigar);
    }

    private NetConnection[] getTcpConnections(Sigar sigar) {
        NetConnection[] tcpConnections = new NetConnection[0];
        try {
            tcpConnections = sigar.getNetConnectionList(NetFlags.CONN_CLIENT | NetFlags.CONN_SERVER | NetFlags.CONN_TCP);
            log.info("Total TCP connections: {} ", tcpConnections.length);
        } catch (SigarException e) {
            log.error("Error retrieving TCP connections: {}", e.getMessage());
        }
        return tcpConnections;
    }

    private NetConnection[] getUdpConnections(Sigar sigar) {
        NetConnection[] udpConnections = new NetConnection[0];
        try {
            udpConnections = sigar.getNetConnectionList(NetFlags.CONN_CLIENT | NetFlags.CONN_SERVER | NetFlags.CONN_UDP);
            log.info("Total UDP connections: {} ", udpConnections.length);
        } catch (SigarException e) {
            log.error("Error retrieving UDP connections: {}", e.getMessage());
        }
        return udpConnections;
    }

    public List<Connection> getEstablishedTcpConnections() {
        List<Connection> connections = new ArrayList<>();
        if (tcpConnections.length > 0) {
            List<NetConnection> establishedTcpConnections = Arrays.asList(tcpConnections).stream()
                    .filter(conn -> conn.getState() == NetFlags.TCP_ESTABLISHED)
                    .collect(Collectors.toList());
            connections = populateConnections(establishedTcpConnections);
        }
        log.info("Total Established TCP Connections: {} ", connections.size());
        return connections;
    }

    public List<Connection> getListeningTcpConnections() {
        List<Connection> connections = new ArrayList<>();
        if (tcpConnections.length > 0) {
            List<NetConnection> listeningTcpConnections = Arrays.asList(tcpConnections).stream()
                    .filter(conn -> conn.getState() == NetFlags.TCP_LISTEN)
                    .collect(Collectors.toList());
            connections = populateConnections(listeningTcpConnections);
        }
        log.info("Total Listening TCP Connections: {} ", connections.size());
        return connections;
    }

    public List<NetworkInterface> getNetworkInterfaces(Sigar sigar) {
        List<NetworkInterface> networkInterfaceList = new ArrayList<>();
        try {
            for (String itrface : sigar.getNetInterfaceList()) {
                NetInterfaceConfig config = sigar.getNetInterfaceConfig(itrface);
                if (!config.getAddress().equals(NetFlags.ANY_ADDR)) {
                    NetworkInterface networkInterface = new NetworkInterface();
                    networkInterface.setName(config.getName());
                    networkInterface.setType(config.getType());
                    networkInterface.setAddress(config.getAddress());
                    log.info(networkInterface.toString());
                    networkInterfaceList.add(networkInterface);
                }
            }
        } catch (SigarException e) {
            log.error("Error retrieving Network Interface List: {}", e.getMessage());
        }
        return networkInterfaceList;
    }

    private List<Connection> populateConnections(List<NetConnection> netConnections) {
        List<Connection> connections = new ArrayList<>();
        for (NetConnection netConnection : netConnections) {
            Connection conn = new Connection();
            conn.setLocalAddress(netConnection.getLocalAddress());
            conn.setLocalPort(String.valueOf(netConnection.getLocalPort()));
            conn.setRemoteAddress(netConnection.getRemoteAddress());
            conn.setRemotePort(String.valueOf(netConnection.getRemotePort()));
            log.info("Connection: {} ", conn);
            connections.add(conn);
        }
        return connections;
    }

    public String getfQDN() {
        return fQDN;
    }

    public String getDefaultGateway() {
        return defaultGateway;
    }

    public String getDomainName() {
        return domainName;
    }

    public int getTotalInboundConnections() {
        return totalInboundConnections;
    }

    public int getTotalOutboundConnections() {
        return totalOutboundConnections;
    }

    public int getTotalInboundTCPConnections() {
        return totalInboundTCPConnections;
    }

    public int getTotalOutboundTCPConnections() {
        return totalOutboundTCPConnections;
    }

    public int getTotalEstablishedTCPConnections() {
        return totalEstablishedTCPConnections;
    }

    public int getTotalClosedTCPConnections() {
        return totalClosedTCPConnections;
    }
}