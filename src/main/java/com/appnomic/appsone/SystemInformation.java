package com.appnomic.appsone;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.ComputerSystem;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.software.os.OperatingSystem;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Data
public class SystemInformation {

    private static final Logger log = LoggerFactory.getLogger(SystemInformation.class);

    private String osName;
    private String osVendor;
    private String osVersion;
    private String osArch;
    private String hostname;
    private String motherBoardSerialNumber;
    private String motherBoardUUID;
    private String processorSerialNumber;
    private int processorCount;
    private SystemInfo systemInfo = new SystemInfo();
    private OperatingSystem operatingSystem = systemInfo.getOperatingSystem();
    private HardwareAbstractionLayer hardwareAbstractionLayer = systemInfo.getHardware();
    private CentralProcessor centralProcessor = hardwareAbstractionLayer.getProcessor();
    private ComputerSystem computerSystem = hardwareAbstractionLayer.getComputerSystem();

    public SystemInformation() {
        this.osName = System.getProperty("os.name");
        log.info("osName: {} ", osName);
        this.osVendor = operatingSystem.getManufacturer();
        log.info("osVendor: {} ", osVendor);
        this.osVersion = System.getProperty("os.version");
        log.info("osVersion: {} ", osVersion);
        this.osArch = System.getProperty("os.arch");
        log.info("osArch: {} ", osArch);
        this.motherBoardSerialNumber = computerSystem.getSerialNumber();
        log.info("motherBoardSerialNumber: {} ", motherBoardSerialNumber);
        this.motherBoardUUID = computerSystem.getHardwareUUID();
        log.info("motherBoardUUID: {} ", motherBoardUUID);
        this.processorSerialNumber = centralProcessor.getProcessorIdentifier().getProcessorID();
        log.info("processorSerialNumber: {} ", processorSerialNumber);
        this.processorCount = centralProcessor.getLogicalProcessorCount();
        log.info("processorCount: {} ", processorCount);
        try {
            this.hostname = InetAddress.getLocalHost().getHostName();
            log.info("hostname: {} ", hostname);
        } catch (UnknownHostException e) {
            log.error("error retrieving hostname: {} ", e.getMessage());
        }
    }
}