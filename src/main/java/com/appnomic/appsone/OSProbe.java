package com.appnomic.appsone;

import com.opencsv.CSVWriter;
import kamon.sigar.SigarProvisioner;
import org.hyperic.sigar.Sigar;
import org.jutils.jprocesses.JProcesses;
import org.jutils.jprocesses.model.ProcessInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class OSProbe {

    private static final Logger log = LoggerFactory.getLogger(OSProbe.class);

    private static Sigar sigar;

    public static void main(String[] args) {
        try {
            SigarProvisioner.provision();
            sigar = new Sigar();
            log.info("Sigar loaded successfully, Process Id(pid)= {} " + sigar.getPid());
        } catch (Exception e) {
            log.error("Failed to load Sigar Library: {} ", e.getMessage());
        }
        List<String[]> csvRow = new ArrayList<>();
        List<String> csvData = new ArrayList<>();
        String[] columnNames = {"OS_NAME", "OS_VERSION", "OS_ARCH", "OS_VENDOR", "COMP_SNO", "COMP_UUID", "NUM_PROCESSORS", "PROCESSOR_ID", "HOSTNAME",
                "USER_NAME", "USER_COUNTRY", "USER_LANGUAGE", "USER_HOME", "USER_TIMEZONE",
                "FQDN", "DEFAULT GATEWAY", "DOMAIN NAME",
                "TOTAL_INBOUND_CONNECTIONS", "TOTAL_OUTBOUND_CONNECTIONS",
                "TOTAL_TCP_INBOUND_CONNECTIONS", "TOTAL_TCP_OUTBOUND_CONNECTIONS",
                "TOTAL_TCP_ESTABLISHED_CONNECTIONS", "TOTAL_TCP_CLOSED_CONNECTIONS",
                "TOTAL_PORTS_FREE", "TOTAL_PORTS_USED",
                "TOTAL_RAM_ALLOCATED", "PHYSICAL_RAM_USED", "PHYSICAL_RAM_FREE",
                "TOTAL_SWAP_SPACE_ALLOCATED", "SWAP_SPACE_USED", "SWAP_SPACE_FREE"};
        csvRow.add(columnNames);

        log.info("------------------");
        log.info("Retrieving System information...");

        SystemInformation systemInfo = new SystemInformation();
        csvData.add(systemInfo.getOsName());
        csvData.add(systemInfo.getOsVersion());
        csvData.add(systemInfo.getOsArch());
        csvData.add(systemInfo.getOsVendor());
        csvData.add(systemInfo.getMotherBoardSerialNumber());
        csvData.add(systemInfo.getMotherBoardUUID());
        csvData.add(String.valueOf(systemInfo.getProcessorCount()));
        csvData.add(systemInfo.getProcessorSerialNumber());
        csvData.add(systemInfo.getHostname());

        log.info("------------------");
        log.info("Retrieving user information...");

        UserInformation userInfo = new UserInformation();
        csvData.add(userInfo.getUserName());
        csvData.add(userInfo.getUserCountry());
        csvData.add(userInfo.getUserLanguage());
        csvData.add(userInfo.getUserHomeDirectory());
        csvData.add(userInfo.getUserTimezone());

        log.info("------------------");
        log.info("Retrieving network information...");

        List<String> gatheredConnections = new ArrayList<>();

        Network network = new Network(sigar);

        List<Connection> establishedTcpConnections = network.getEstablishedTcpConnections();
        for (Connection conn : establishedTcpConnections) {
            gatheredConnections.add(conn.toString());
        }
        List<Connection> listeningTcpConnections = network.getListeningTcpConnections();
        for (Connection conn : listeningTcpConnections) {
            gatheredConnections.add(conn.toString());
        }

        csvData.add(network.getfQDN());
        csvData.add(network.getDefaultGateway());
        csvData.add(network.getDomainName());

        csvData.add(String.valueOf(network.getTotalInboundConnections()));
        csvData.add(String.valueOf(network.getTotalOutboundConnections()));
        csvData.add(String.valueOf(network.getTotalInboundTCPConnections()));
        csvData.add(String.valueOf(network.getTotalOutboundTCPConnections()));
        csvData.add(String.valueOf(network.getTotalEstablishedTCPConnections()));
        csvData.add(String.valueOf(network.getTotalClosedTCPConnections()));

        int totalConnections = gatheredConnections.size() - 1;

        log.info("------------------");
        log.info("Scanning ports 0->65536 ...");

        PortScanner portScanner = new PortScanner();

        csvData.add(String.valueOf(portScanner.getFreePortsCount()));
        csvData.add(String.valueOf(portScanner.getUsedPorts().size()));
        //csvData.add(portScanner.getUsedPorts().toString());

        log.info("------------------");
        log.info("Retrieving Network Interfaces...");

        List<NetworkInterface> networkInterfaceList = network.getNetworkInterfaces(sigar);

        //csvData.add(networkInterfaceList.toString());

        log.info("------------------");
        log.info("Retrieving Memory Information...");

        Memory memory = new Memory(sigar);

        csvData.add(String.valueOf(memory.getTotalRAM()));
        csvData.add(String.valueOf(memory.getTotalRAMUsed()));
        csvData.add(String.valueOf(memory.getTotalRAMFree()));

        csvData.add(String.valueOf(memory.getTotalSwapSpace()));
        csvData.add(String.valueOf(memory.getTotalSwapSpaceUsed()));
        csvData.add(String.valueOf(memory.getTotalSwapSpaceFree()));


        log.info("------------------");
        log.info("Retrieving volumes/disk/partitions...");

        for (Path path : FileSystems.getDefault().getRootDirectories()) {
            try {
                FileStore store = Files.getFileStore(path);
                log.info("Path: {} , Available: {} , Total: {} ", path, store.getUsableSpace(), store.getTotalSpace());
            } catch (IOException e) {
                System.out.println("error querying space: " + e.toString());
            }
        }

        String[] values = new String[csvData.size()];
        for (int i = 0; i < csvData.size(); i++) { //convert the csvData into array of strings
            values[i] = csvData.get(i);
        }

        csvRow.add(values);

        csvRow.add(new String[csvData.size()]);   // add an empty row in csv
        csvRow.add(new String[csvData.size()]);  // add an empty row in csv

        String[] anotherHeader = new String[csvData.size()];

        anotherHeader[0] = "RUNNING_PROCESSES";
        anotherHeader[4] = "TCP_CONNECTIONS";
        csvRow.add(anotherHeader);

        log.info("------------------");
        log.info("Retrieving running processes...");

        List<ProcessInfo> processesList = JProcesses.getProcessList();

        for (final ProcessInfo processInfo : processesList) {
            if (processInfo.getCommand() != null && !processInfo.getCommand().equals("")) { // filtering out processes which started without arguments
                log.info("Process: {} ", processInfo);
                String[] newRow = new String[csvData.size()];
                newRow[0] = processInfo.toString();
                if (totalConnections > 0) {                // print out the connections in a subsequent column to process column
                    newRow[4] = gatheredConnections.get(totalConnections);
                    totalConnections = totalConnections - 1;
                }
                csvRow.add(newRow);
            }
        }

        try {
            FileWriter fileWriter = new FileWriter("./os-probe-result.csv");
            CSVWriter csvWriter = new CSVWriter(fileWriter);
            csvWriter.writeAll(csvRow);
            csvWriter.flush();
            fileWriter.flush();
        } catch (Exception e) {
            log.info("Error creating csv file: {}" + e.getMessage());
        }
    }
}
