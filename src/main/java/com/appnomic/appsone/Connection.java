package com.appnomic.appsone;

import lombok.Data;

@Data
public class Connection {
    String localAddress;
    String localPort;
    String remoteAddress;
    String remotePort;

    public String toString(){
        return "[ local:"+this.localAddress + ":" + localPort + " , remote:"+remoteAddress + ":" + remotePort + " ]";
    }
}
