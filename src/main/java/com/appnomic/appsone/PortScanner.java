package com.appnomic.appsone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public class PortScanner {

    private static final Logger log = LoggerFactory.getLogger(PortScanner.class);

    private int freePortsCount = 0;

    private List<Integer> usedPorts = new ArrayList<>();

    public PortScanner() {
        for (int i = 0; i < 65536; i++) {
            try {
                new ServerSocket(i).close();
                freePortsCount = freePortsCount + 1;
            } catch (Exception e) {
                usedPorts.add(i);
            }
        }
        log.info("Total ports Free: {} ", freePortsCount);
        log.info("Total ports Used: {} ", usedPorts.size());
        log.info("Ports Used: {} ", usedPorts.toString());
    }

    public int getFreePortsCount() {
        return freePortsCount;
    }

    public List<Integer> getUsedPorts() {
        return usedPorts;
    }
}
